'use strict';

// 引入需要的各种模块
let Koa = require('koa');
// let path = require('path');
let bodyParser = require('koa-bodyparser');
let controller = require('./controllers');
let templating=require('./templating');
let staticRes=require('koa-static');
let session = require('koa-session');
let models = require('./models');


// 初始化一个服务器实例
let app = new Koa();

app.use(staticRes(__dirname+'/statics'));
   
  // 配置session的中间件
  app.keys = ['ppz']; /** cookie的签名 */
  const CONFIG = {
    key: 'koa:sess', /** 默认 */
    maxAge: 86400000, /** cookie的过期时间 【需要修改】*/
    overwrite: true, /** (boolean) can overwrite or not (default true) 没有效果 默认*/
    httpOnly: true, /** true表示只有服务器端可以获取cookie*/
    signed: true, /** 默认 签名 */
    rolling: false, /** 在每次请求时强行设置 cookie,这将重置 cookie 过期时间(默认：false) 【需要修改】*/
    renew: true, /** (boolean) renew session when session is nearly expired 【需要修改】*/
  }
  app.use(session(CONFIG, app));

// 注册中间件 注意先注册post请求的解释器，后注册路由
app.use(bodyParser());

app.use(templating);

app.use(controller());

//初始化数据表
(async () => {
    await models.sync();
})();

// 定义端口，设置监听
let port = 4200;
app.listen(port);

// 打印服务器信息
console.log(`http://127.0.0.1:${port}`);