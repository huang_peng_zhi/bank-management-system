function transfer() {
    let html=`\<\div style="padding:20px;">
    卡号：<input type="text" id="cardNo">
    <p></p>
    <br>
    数额：<input type="text" id="transfer">
    \<\/div>
    `
    layer.open({
        type: 1,
        title: '转账',
        area: ['300px', '250px'],
        offset: '200px',
        shadeClose: true, //点击遮罩关闭
        content: html,
        btn: ['转账','关闭'],
        yes:function (index) {
            axios({
                method: 'post',
                url: '/transfer',
                data: {              
                    cardNo:$('#cardNo').val(),
                    transfer: $('#transfer').val(),
                }
            }).then(function (response) {
                // axios工具下，服务器真正返回的数据在返回对象的data中
                let res = response.data;
                if (res.code === 200) {
                    layer.msg(res.msg,{time:1500});
                    setTimeout(`layer.close(${index})`,1900)
                }else if(res.code===404){
                    layer.msg(res.msg,{time:1500});
                }
            }).catch((err) => {
                layer.msg('转账失败!请查询数额后再次尝试')
                console.log(err);
            });
        }
     });
}