function remove() {
    var html = `\<\div style="padding:20px;">
    确定删除您的这张银行卡吗？请慎重选择！！！！！
    <br>
    \<\/div>`;

    layer.open({
        type: 1,
        title: '销卡',
        area: ['300px', '390px'],
        offset: '200px',
        shadeClose: true, //点击遮罩关闭
        content: html,
        btn: ['确定', '取消'],
        yes: function (index) {
            axios({
                method: 'post',
                url: '/deleteCarDone',
                data: {
                    CardState:$('#CardState').val(),
                }
            }).then(function (response) {
                // axios工具下，服务器真正返回的数据在返回对象的data中
                let res = response.data;
                if (res.code === 200) {
                    layer.msg(res.msg,{time:1500});
                    setTimeout(`layer.close(${index})`,1900)
                }else if(res.code===500){
                    layer.msg(res.msg,{time:1500});
                    setTimeout(`layer.close(${index})`,1900)
                }else if(res.code===404){
                    layer.msg(res.msg,{time:1500});
                }
            }).catch((err) => {
                layer.msg('销卡失败!')
            });
        }
    });
}
