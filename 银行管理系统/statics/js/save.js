function saved() {
    let html=`\<\div style="padding:20px;">
    卡号：<input type="text" id="cardNo">
    <p></p>
    <br>
    数额：<input type="text" id="moneyNew">
    \<\/div>
    `
    layer.open({
        type: 1,
        title: '存款',
        area: ['300px', '250px'],
        offset: '200px',
        shadeClose: true, //点击遮罩关闭
        content: html,
        btn: ['存入','关闭'],
        yes:function (index) {
            axios({
                method: 'post',
                url: '/save',
                data: {
                    cardNo:$('#cardNo').val(),
                    moneyNew: $('#moneyNew').val(),
                }
            }).then(function (response) {
                // axios工具下，服务器真正返回的数据在返回对象的data中
                let res = response.data;
                if (res.code === 200) {
                    layer.msg(res.msg,{time:1500});
                    setTimeout(`layer.close(${index})`,1900)
                }else if(res.code===404){
                    layer.msg(res.msg,{time:1500});
                }
            }).catch((err) => {
                layer.msg('存款失败!')
            });
        }
     });
}