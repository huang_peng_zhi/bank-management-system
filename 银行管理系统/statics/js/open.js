function opened() {
    var html = `\<\div style="padding:20px;">
    真实姓名：<input type="text" id="realName">
    <p></p>
    <br>
    手机号码：<input type="text" id="phoneNumber">
    <p></p>
    <br>
    预存金额：<input type="text" id="advanceDeposit">
    <p></p>
    <br>
    交易密码：<input type="password" id="transactionPwd">
    \<\/div>`;

    layer.open({
        type: 1,
        title: '开户',
        area: ['300px', '390px'],
        offset: '200px',
        shadeClose: true, //点击遮罩关闭
        content: html,
        btn: ['开户', '关闭'],
        yes: function (index) {
            axios({
                method: 'post',
                url: '/add',
                data: {
                    realName:$('#realName').val(),
                    phoneNumber:$('#phoneNumber').val(),
                    advanceDeposit: $('#advanceDeposit').val(),
                    transactionPwd: $('#transactionPwd').val(),
                }
            }).then(function (response) {
                // axios工具下，服务器真正返回的数据在返回对象的data中
                let res = response.data;
                if (res.code === 200) {
                    layer.msg(res.msg,{time:1500});
                    setTimeout(`layer.close(${index})`,1900)
                }else if(res.code===500){
                    layer.msg(res.msg,{time:1500});
                    setTimeout(`layer.close(${index})`,1900)
                }else if(res.code===404){
                    layer.msg(res.msg,{time:1500});
                }
            }).catch((err) => {
                layer.msg('开户失败!')
            });
        }
    });
}
