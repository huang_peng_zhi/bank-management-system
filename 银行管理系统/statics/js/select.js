function selected() {
    let html = `\<\div style="padding:20px;">
    卡号：<b id="cardNo"></b>
    <p></p>
    <br>
    余额：<b id="money"></b>
    \<\/div>
    `
    layer.open({
        type: 1,
        title: '名下信息',
        area: ['300px', '210px'],
        offset: '200px',
        shadeClose: true, //点击遮罩关闭
        content: html,
        btn: ['查询', '关闭'],
        yes: function (index) {
            request('/find', 'GET', '', '', res => {
                if (res.code === 200) {
                    $('#cardNo').append(`${res.selectCard[0].CardNo}`);
                    $('#money').append(`${res.selectCard[0].CardBalance}`)
                    layer.msg(res.msg, { time: 1500 });
                } else if (res.code === 404) {
                    layer.msg(res.msg, { time: 1500 });
                    setTimeout(`layer.close(${index})`, 1900)
                }
            }, err => {
                layer.msg(err)
            })
        }
    });
}