let {defineModel,DataTypes}=require('../db');


let model=defineModel('CardExchange',{
    UserId:{
        type:DataTypes.BIGINT,
        // primaryKey:true,
        // autoIncrement:true
    },
    CardNo:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    MoneyInBank:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    MoneyOutBank:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    ExchangeTime:{
        type:DataTypes.STRING,
        allowNull:false
    },
});

module.exports=model;