let { defineModel, DataTypes } = require('../db');


let model = defineModel('AccountInfo', {
    UserId: {
        type: DataTypes.BIGINT,
        // primaryKey:true,
        // autoIncrement:true
    },
    AccountCode: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    AccountPhone: {
        type: DataTypes.STRING(80),
        allowNull: false
    },
    RealName: {
        type: DataTypes.STRING(80),
        allowNull: false
    },
    OpenTime: {
        type: DataTypes.STRING,
        allowNull: false
    },
});

module.exports = model;