let {defineModel,DataTypes}=require('../db');


let model=defineModel('users',{
    username:{
        type:DataTypes.STRING(80),
        allowNull:false
    },
    password:{
        type:DataTypes.STRING(80),
        allowNull:false
    },
});

module.exports=model;