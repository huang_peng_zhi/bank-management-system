'use strict';
let fs=require('fs');
let {sequelize}=require('../db');

// 搜索所有的模型定义文件
function searchFiles(){
    let files=fs.readdirSync(__dirname);
    return files.filter(name=>{
        return name.endsWith('.js') && name!=='index.js';
    });
}

// 注册(引入)所有的模型到指定对象
function registerModel(files){
    let obj={};

    files.forEach(name=>{
        let key=name.substring(0,name.length-3);
        obj[key]=require(__dirname + '/'+name);
    })
    console.log(obj);
    return obj;
}

let files=searchFiles();

let obj=registerModel(files);

// 给这个对象的属性sync绑定一个方法
obj.sync=async ()=>{
    if(process.env==='product'){
        console.log('当前是生产环境，不能强制修改数据表或者删除数据表');
    }else{
        return sequelize.sync({force:true});
    }
    
}

// 暴露这个带着很多模型的对象
module.exports=obj;