let {defineModel,DataTypes}=require('../db');


let model=defineModel('CardTransfer',{
    TransferId:{
        type:DataTypes.BIGINT,
        // primaryKey:true,
        // autoIncrement:true
    },
    CardNoOut:{
        type:DataTypes.STRING(80),
        allowNull:false
    },
    CardNoIn:{
        type:DataTypes.STRING(80),
        allowNull:false
    },
    TransferMoney:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    TransferTime:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
});

module.exports=model;