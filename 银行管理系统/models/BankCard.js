let { defineModel, DataTypes } = require('../db');


let model = defineModel('BankCard', {
    CardNo: {
        type: DataTypes.BIGINT,
        // primaryKey:true,
        // autoIncrement:true
    },
    CardBalance: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    UserId: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    CardPwd: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    CardState: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    CardTime: {
        type: DataTypes.STRING,
        allowNull: false
    },
});

module.exports = model;